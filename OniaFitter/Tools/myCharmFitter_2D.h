#ifndef MYCHARMFITTER_H
#define MYCHARMFITTER_H
#include "RooPlot.h"
#include "RooFitResult.h"
using namespace RooFit;

struct FitFraction {
	double fsignal;
	double fsigPrompt;
	double fracPPsi2S;
	double fracNPPsi2S;
	double fbkg1;
	double fbkg2;
};

struct FitValue {

	double Inclusive1S;
	double Inclusive1S_Err;
	double Prompt1S;
	double Prompt2S;
	double Prompt1S_Err;
	double Prompt2S_Err;
	double Inclusive2S;
	double Inclusive2S_Err;
	double NonPrompt1S;
	double NonPrompt2S;
	double NonPrompt1S_Err;
	double NonPrompt2S_Err;
	double NPFraction1S;
	double NPFraction2S;
	double NPFraction1S_Err;
	double NPFraction2S_Err;
	double PromptRatio;
	double PromptRatio_Err;
	double NonPromptRatio;
	double NonPromptRatio_Err;

	double MassSigma1S;
	double MassSigma2S;
	double TauSigma1;
	double TauSigma2;
};

class myCharmFitter {

	public:
		myCharmFitter();
		virtual ~myCharmFitter() {
			if (mPDF_Tau_NP_1S) delete mPDF_Tau_NP_1S;  mPDF_Tau_NP_1S = 0;
			if (mPDF_Tau_NP_2S) delete mPDF_Tau_NP_2S;  mPDF_Tau_NP_2S = 0;
			if (mPDF_Tau_NP_B1) delete mPDF_Tau_NP_B1;  mPDF_Tau_NP_B1 = 0;
			if (mPDF_Tau_NP_B2) delete mPDF_Tau_NP_B2;  mPDF_Tau_NP_B2 = 0;

			if (mPDF_Mass_Signal_1S) 	delete mPDF_Mass_Signal_1S;         mPDF_Mass_Signal_1S = 0;
			if (mPDF_Mass_Signal_2S) 	delete mPDF_Mass_Signal_2S;         mPDF_Mass_Signal_2S = 0;
			if (mPDF_Mass_Bkgd_Prompt) 	delete mPDF_Mass_Bkgd_Prompt;       mPDF_Mass_Bkgd_Prompt = 0;
			if (mPDF_Mass_Bkgd_NonPrompt) 	delete mPDF_Mass_Bkgd_NonPrompt;    mPDF_Mass_Bkgd_NonPrompt = 0;
			if (mPDF_Mass_Bkgd_MisID) 	delete mPDF_Mass_Bkgd_MisID;        mPDF_Mass_Bkgd_MisID = 0;
		}

		RooPlot* GetMassPlot(void) const {return m_mframe;}
		RooPlot* GetMassPlotZ(void) const {return m_mframeZ;}
		RooPlot* GetTimePlot(void) const {return t_mframe;}

		RooPlot* GetMassPullPlot(void) const {return m_pullframe;}
		RooPlot* GetTimePullPlot(void) const {return t_pullframe;}

		RooPlot* GetPre2STimePlot(void) const {return t_mframe_RangePrePsi;}
		RooPlot* Get2STimePlot(void) const {return t_mframe_RangePsi;}
		RooPlot* GetPost2STimePlot(void) const {return t_mframe_RangePostPsi;}
		RooPlot* GetPre1STimePlot(void) const {return t_mframe_RangePreJpsi;}
		RooPlot* Get1STimePlot(void) const {return t_mframe_RangeJpsi;}
		RooPlot* GetPMassPlot(void)  const {return m_mframe_RangeP;}
		RooPlot* GetNPMassPlot(void) const {return m_mframe_RangeNP;}
		RooPlot* GetNP1MassPlot(void) const {return m_mframe_RangeNP1;}
		RooPlot* GetMisIDMassPlot(void) const {return m_mframe_RangeMisID;}

		TH1* GetData(void)  const {return hh_data;}
		TH1* GetData_m(void)  const {return hh_data_m;}
		TH1* GetData_tau(void)  const {return hh_data_tau;}
		TH1* GetPdf(void)  const {return hh_pdf;}
		TH1* GetTF2(void)  const {return hf_pdf;}
		TF1* GetTF1_m_f(void)  const {return hf_pdf_m_f;}
		TH1* GetTF1_m(void)  const {return hf_pdf_m;}
		TH1* GetTF1_tau(void)  const {return hf_pdf_tau;}

		FitFraction GetFrac(void) 	  {return m_frac;}
		FitValue    GetVal(void) 	  {return m_val;}
		// tools
		//bool CheckConvergence(RooFitResult* fr);
		//bool CheckParaError(RooFitResult* fr);
		double GetChi2(void)      	{return m_ReducedChi2;}
		double GetChi2_M(void)      	{return m_ReducedChi2_m;}
		double GetChi2_Tau(void)      	{return m_ReducedChi2_tau;}
		bool IsWeighted()           {return m_is_weighted;}
		void SetExtraPlots(double flag)     {m_extraPlots = flag;}
		void SetWeighted(bool wflag)        {m_is_weighted = wflag;}

		void SetFixDecay(bool flag)         {m_fix_decay = flag;}
		void SetLife1S(double life)         {m_life_1s= life;}
		void SetTauSigma(double sigma)      {m_tau_sigma= sigma;}
		double GetLife1S(void)         	{return m_life_1s;}
		double GetTauSigma(void)      	{return m_tau_sigma;}
		int GetNParams(void)	const	{return nparams;}

		RooFitResult* GetResultNoSumW2(void) {return m_result_NoSumW2;}

		//RooFitResult* Nominal_Fit(RooDataSet* data, RooArgSet* savedParams);
		RooFitResult* Nominal_Fit(bool badFit, RooDataSet* data, RooArgSet* savedParams, TString* inputF = 0, TString* outF = 0, TString* outF_Par = 0, int iy=0, double pt=60, bool angleOn = 0, int sys=0);
		RooFitResult* Nominal_Fit_JPsi(RooDataSet* data, RooArgSet* savedParams, TString* inputF = 0, TString* outF = 0, int life = 0);
		RooFitResult* Variation_Fit(RooDataSet* data, const int type = 0);
		RooFitResult* Binned_Fit(TH1* hist, const int type = 0);
		RooFitResult* mc_Fit(RooDataSet* data);
		//RooFitResult* Sys_Mass_Fit(TH1D* data, int type, FitFraction g_frac);
		//RooFitResult* Sys_Time_Fit(RooDataHist* data, int type, FitFraction g_frac);

	private:
		bool m_debug;
		bool m_extraPlots;
		bool m_is_weighted;
		bool m_fix_decay;
		bool useInput;
		bool drawOnly;

		RooPlot* m_mframe;
		RooPlot* m_mframeZ;
		RooPlot* t_mframe;

		//Pull plots
		RooPlot* m_pullframe;
		RooPlot* t_pullframe;
		// additional plots
		RooPlot* m_mframe_RangeNP;
		RooPlot* m_mframe_RangeNP1;
		RooPlot* m_mframe_RangeP;
		RooPlot* m_mframe_RangeMisID;
		RooPlot* t_mframe_RangePreJpsi;
		RooPlot* t_mframe_RangeJpsi;
		RooPlot* t_mframe_RangePrePsi;
		RooPlot* t_mframe_RangePsi;
		RooPlot* t_mframe_RangePostPsi;

		FitFraction m_frac;
		FitValue    m_val;

		double m_ReducedChi2;
		double m_ReducedChi2_m;
		double m_ReducedChi2_tau;
		double m_life_1s;
		double m_tau_sigma;
		int nparams;

		RooFitResult* m_result;
		RooFitResult* m_result_NoSumW2;

		RooRealVar* tau;
		RooRealVar* m;
		RooRealVar* tau_err;
		RooRealVar* m_err;

		RooResolutionModel* mPDF_Tau_Reso;

		RooAbsPdf* mPDF_Tau_NP_1S;
		RooAbsPdf* mPDF_Tau_NP_2S;
		RooAbsPdf* mPDF_Tau_NP_B1;
		RooAbsPdf* mPDF_Tau_NP_B11;
		RooAbsPdf* mPDF_Tau_NP_B2;

		RooAbsPdf* mPDF_Mass_Signal_1S;
		RooAbsPdf* mPDF_Mass_Signal_2S;
		RooAbsPdf* mPDF_Mass_Bkgd_Prompt;
		RooAbsPdf* mPDF_Mass_Bkgd_NonPrompt;
		RooAbsPdf* mPDF_Mass_Bkgd_MisID;
		RooAbsPdf* mPDF_Mass_Bkgd_MisID1;

		TH1* hh_data;
		TH1* hh_data_m;
		TH1* hh_data_tau;
		TH1* hh_pdf;
		TH1* hf_pdf;
		TH1* hf_pdf_m;
		TF1* hf_pdf_m_f;
		TH1* hf_pdf_tau;
};
#endif
