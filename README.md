// download the package OniaFitter

git clone https://gitlab.cern.ch/tzakarei/inclusivejpsi_lowpt.git

cd OniaFitter

//main working files:
runCharmFit_Combine.C – combining RooDataSets from all years, requesting fit, which is defined in file:
Tools/myCharmFitter_2D.cxx 


// To run the fit, first execute:

setupATLAS

lsetup "root 6.18.04-x86_64-centos7-gcc8-opt"

//for HIGH pT

root -l -b -q "runCharmFit_Combine.C(\"FitResults_23_35_Data_S2test_y0_vStr2_2G.root\",\"HighPt\","0","1","23","35", "1", "0","99")"

root -l -b -q "runCharmFit_Combine.C(\"FitResults_23_35_Data_S2test_y1_vStr2_2G.root\",\"HighPt\","1","2","23","35", "1", "0","99")"

root -l -b -q "runCharmFit_Combine.C(\"FitResults_23_35_Data_S2test_y2_vStr2_2G.root\",\"HighPt\","2","3","23","35", "1", "0","99")"



//for LOW pT

root -l -b -q "runCharmFit_Combine.C(\"FitResults_18_23_Data_S2test_y0_vStr2_2G.root\",\"LowPt\","0","1","18","24", "1", "0","99")"

root -l -b -q "runCharmFit_Combine.C(\"FitResults_14_17_Data_S2test_y0_vStr2_2G.root\",\"LowPt\","0","1","14","18", "1", "0","99")"

root -l -b -q "runCharmFit_Combine.C(\"FitResults_0_13_Data_S2test_y0_vStr2_2G.root\",\"LowPt\","0","1","0","14", "1", "0","99")"



root -l -b -q "runCharmFit_Combine.C(\"FitResults_18_23_Data_S2test_y1_vStr2_2G.root\",\"LowPt\","1","2","18","24", "1", "0","99")"

root -l -b -q "runCharmFit_Combine.C(\"FitResults_14_17_Data_S2test_y1_vStr2_2G.root\",\"LowPt\","1","2","14","18", "1", "0","99")"

root -l -b -q "runCharmFit_Combine.C(\"FitResults_0_13_Data_S2test_y1_vStr2_2G.root\",\"LowPt\","1","2","0","14", "1", "0","99")"



root -l -b -q "runCharmFit_Combine.C(\"FitResults_18_23_Data_S2test_y2_vStr2_2G.root\",\"LowPt\","2","3","18","24", "1", "0","99")"

root -l -b -q "runCharmFit_Combine.C(\"FitResults_14_17_Data_S2test_y2_vStr2_2G.root\",\"LowPt\","2","3","14","18", "1", "0","99")"

root -l -b -q "runCharmFit_Combine.C(\"FitResults_0_13_Data_S2test_y2_vStr2_2G.root\",\"LowPt\","2","3","0","14", "1", "0","99")"




////////// general definition ////////

root -l -b -q "runCharmFit_Combine.C(\"par1\","par2","par3","par4","par5", "par6", "par7")"


par1 – output file name, 

par2 – pT range: either "LowPt" or "HighPt", 

par3 – y low range inex,

par4 – y high range inex, 

par5 – pt low range inex,

par6 – pt high range inex, 

par7 – angle (if =1 –  correlation is on, if = 0 – correlation is off), 

par8 - systematics (when = 0 means that systematics are not considered),

par9 - sub bin number to fit, if you want to fit all sub bins put 99.
